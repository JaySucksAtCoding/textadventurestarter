package items;

import textadventure.World;

public class Trap extends Item {

	private String message;
	
	public Trap(World world, String name, int weight, boolean takeable, String description, String message) {
		super(world, name, weight, takeable, description);
		this.message=message;
	}

	@Override
	public void doUse() {
		World.print(message + " GAME OVER\n\n");
		getWorld().setGameOver(true);
	}
	
	

}
