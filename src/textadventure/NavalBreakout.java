package textadventure;

import java.util.Calendar;
import java.util.GregorianCalendar;

import items.Book;
import items.CloseableContainer;
import items.Clothes;
import items.Escape;
import items.Item;
import items.Keys;
import items.Lantern;
import items.LockableContainer;
import items.Maker;
import items.Scenery;
import items.Special;
import items.Thermite;
import items.Torch;
import items.Trap;
import items.UselessItem;

public class NavalBreakout extends World {

	private static final String CELL="Cell";
	private static final String HALLWAY="Hallway";
	private static final String OFFICE="Office";
	private static final String BALCONY="Balcony";
	private static final String CQ="Crew's Quarters";
	private static final String STORAGE="Storage";
	private static final String DECK="Deck";
	
	@Override
	public void initializeGame() {
		createRooms();
		createPlayer();
	}

	public void createPlayer() {
		setPlayer(new Player(getRoom(CELL), this, "Player", "Time to go back home.", 0));
		getPlayer().addItem(new Keys(this, "white_key", 0, Item.TAKEABLE, "A white key."));
		getPlayer().addItem(new Keys(this, "black_key", 0, Item.TAKEABLE, "A black key."));
		getPlayer().addItem(new Keys(this, "red_key", 0, Item.TAKEABLE, "A red key."));
		getPlayer().addItem(new Keys(this, "blue_key", 0, Item.TAKEABLE, "A blue key."));
		getPlayer().addItem(new Keys(this, "green_key", 0, Item.TAKEABLE, "A green key."));
		getPlayer().addItem(new Keys(this, "yellow_key", 0, Item.TAKEABLE, "A yellow key."));
		getPlayer().addItem(new Keys(this, "silver_key", 0, Item.TAKEABLE, "A silver key."));
		getPlayer().addItem(new Keys(this, "gold_key", 0, Item.TAKEABLE, "A gold key."));
		getPlayer().addItem(new Keys(this, "glass_key", 0, Item.TAKEABLE, "A glass key."));
		getPlayer().addItem(new Keys(this, "metal_key", 0, Item.TAKEABLE, "A metal key."));
		getPlayer().addItem(new Keys(this, "wooden_key", 0, Item.TAKEABLE, "A wooden key."));
		getPlayer().addItem(new UselessItem(this, "clothes", 0, Item.TAKEABLE, "Your clothes."));
		getPlayer().addItem(new Special(this, "boots", 0, Item.TAKEABLE, "Your boots", new Keys(this, "pin", 0, Item.TAKEABLE, "A small pin.")));
	}

	private void createRooms() {
		addRoom(new Room(CELL, "You are in a small cell lit by a single torch fastened to a wall.\n"
				+ "There is a steel door on the starboard side of the ship and a small\n"
				+ "window the size of your face that looks out the port side of the ship.\n\n", this));
		addRoom(new Room(HALLWAY, "You can see a set of stairs leading up to somewhere towards the\n"
				+ "bow of the ship, but you can�t see where because the exit is blocked\n"
				+ "by a locked trapdoor. On the starboard side, there is a set of stairs\n"
				+ "leading down, also locked behind a trapdoor. You can see barrels along\n"
				+ "one of the walls as well as some crates.\n\n", Room.LOCKED, this, "red_key"));
		addRoom(new Room(OFFICE, "You find yourself in the office of the captain of this ship. The\n"
				+ "floor creaks as you walk around and you notice that some floorboards\n"
				+ "are quite loose. You can see maps, books, a desk with what seems to be\n"
				+ "documents and three cabinets. Towards the stern of the ship, there is\n"
				+ "a doorway to a small balcony.\n\n", Room.LOCKED, this, "glass_key"));
		addRoom(new Room(BALCONY, "You are on a small balcony and the port gets smaller and smaller\n"
				+ "behind you. The wind blows on your face and some seawater occasionally\n"
				+ "splashes on you. There is nothing except for a lamp loosely fastened to\n"
				+ "the hull of the ship.\n\n", Room.LOCKED, this, "pin"));
		addRoom(new Room(CQ, "The walls of this room are lined with bunk beds and locked cabinets for\n"
				+ "the crew of this ship. In one corner, you see a larger bed and a chest,\n"
				+ "probably for the captain of this ship. That bed is larger and looks\n"
				+ "extremely comfortable. Towards the stern of the ship, you spot a ladder\n"
				+ "leading down with a sign that reads \"storage\n.\n\n", Room.LOCKED, this, "silver_key"));
		addRoom(new Room(STORAGE, "The storage room has no lighting whatsoever and you can�t see\n"
				+ "anything. You can vaguely make out the shapes of barrels, crates, and chests,\n"
				+ "but you can�t tell where they are.\n\n", this));
		getRoom(STORAGE).setLit(false);
		addRoom(new Room(DECK, "You are on the deck of the ship. There are other men on the deck,\n"
				+ "but they don�t recognize you as they are busy working the ship. You\n"
				+ "make an effort to not disturb them, as that may jeopardize your escape.\n"
				+ "You can see everything that one would expect to see on a deck of a ship:\n"
				+ "masts, ropes, crates, barrels, cannons, and lifeboats. However, you can\n"
				+ "not approach many of these because of the busy crewmen.\n\n", Room.LOCKED, this, "hahaha you can't unlock this"));
		
		getRoom(CELL).setExits(getRoom(HALLWAY), null, null, null);
		getRoom(HALLWAY).setExits(getRoom(CQ), getRoom(OFFICE), getRoom(CELL), getRoom(DECK));
		getRoom(CQ).setExits(null, getRoom(STORAGE), getRoom(HALLWAY), null);
		getRoom(STORAGE).setExits(null, null, null, getRoom(CQ));
		getRoom(OFFICE).setExits(null, getRoom(BALCONY), null, getRoom(HALLWAY));
		getRoom(BALCONY).setExits(null, null, null, getRoom(OFFICE));
		getRoom(DECK).setExits(null, getRoom(HALLWAY), null, null);
		
		UselessItem table=new UselessItem(this, "table", 0, Item.NOT_TAKEABLE, "A simple, wooden table.");
		UselessItem stool=new UselessItem(this, "stool", 0, Item.TAKEABLE, "A wooden three-legged stool.");
		UselessItem hay=new UselessItem(this, "hay", 0, Item.NOT_TAKEABLE, "A stack of hay for you to sleep on. It is very uncomfortable.");
		Torch torch=new Torch(this, "torch", 0,  Item.NOT_TAKEABLE, "A lit torch that burns bright, providing light to your cell.");
		getRoom(CELL).addItem(table);
		getRoom(CELL).addItem(stool);
		getRoom(CELL).addItem(hay);
		getRoom(CELL).addItem(torch);
		
		Trap knife=new Trap(this, "knife", 0, Item.TAKEABLE, "A sharp butter knife.", "While you were swinging your knife, it flew out of your\n"
				+ "hand, hit the wall, bounced back, and landed in your chest. You lie in\n"
				+ "agony as you slowly bleed out.");
		CloseableContainer cont1=new CloseableContainer(this, "left_crate", 0, Item.NOT_TAKEABLE, "A wooden crate that is too big for you to take", false);
		cont1.addItem(knife);
		CloseableContainer cont2=new CloseableContainer(this, "right_crate", 0, Item.NOT_TAKEABLE, "A wooden crate that is too big for you to take", false);
		UselessItem smiley=new UselessItem(this, "smiley_face", 0, Item.TAKEABLE, "A smiley face sticker.");
		LockableContainer cont3=new LockableContainer(this, "black_crate", 0, Item.NOT_TAKEABLE, "A black crate that is too big for you to take", false, true, "white_key");
		cont3.addItem(smiley);
		CloseableContainer cont4=new CloseableContainer(this, "barrel", 0, Item.NOT_TAKEABLE, "A wooden barrel that is too big for you to take.", false);
		getRoom(HALLWAY).addItem(cont1);
		getRoom(HALLWAY).addItem(cont2);
		getRoom(HALLWAY).addItem(cont3);
		getRoom(HALLWAY).addItem(cont4);
		
		UselessItem map=new UselessItem(this, "map", 0, Item.NOT_TAKEABLE, "The map shows Niobe, your home, and Aftokrater, the prison where you are being transported to.");
		Book exbk=new Book(this, "red_book", 0, Item.TAKEABLE, "A thin, red book.");
		exbk.setText("Thermite is a pyrotechnic composition of aluminum, iron oxide, and magnesium.\n"
				+ "It burns at 2000C, hot enough to cut through nearly any barrier known to man.");
		UselessItem desk=new UselessItem(this, "desk", 0, Item.NOT_TAKEABLE, "There are documents that outline your crime and prison sentence on the desk.");
		Special floor=new Special(this, "floor", 0, Item.NOT_TAKEABLE, "The floor squeaks on certain spots", new UselessItem(this, "iron_oxide", 0, Item.TAKEABLE, "A handful of iron oxide"));
		LockableContainer cont5=new LockableContainer(this, "left_cabinet", 0, Item.NOT_TAKEABLE, "A wooden cabinet.",false, true, "blue_key");
		LockableContainer cont6=new LockableContainer(this, "middle_cabinet", 0, Item.NOT_TAKEABLE, "A wooden cabinet.", false, true, "nada. nothing. You're not opening this one.");
		CloseableContainer cont7=new CloseableContainer(this, "right_cabinet", 0, Item.NOT_TAKEABLE, "A wooden cabinet.", false);
		LockableContainer sbox=new LockableContainer(this, "small_box", 0, Item.TAKEABLE, "A mysterious small, locked box.", false, true, "another unlockable item.");
		cont7.addItem(sbox);
		cont5.addItem(exbk);
		getRoom(OFFICE).addItem(map);
		getRoom(OFFICE).addItem(desk);
		getRoom(OFFICE).addItem(floor);
		getRoom(OFFICE).addItem(cont5);
		getRoom(OFFICE).addItem(cont6);
		getRoom(OFFICE).addItem(cont7);
		
		Lantern lantern=new Lantern(this, "lantern", 0, Item.NOT_TAKEABLE, "A small lamp fastened to the hull of the ship.");
		Scenery ocean=new Scenery(this,"ocean", 0, "You watch as your home disappears into the distance and wonder, 'Will I ever make it back?'");
		getRoom(BALCONY).addItem(lantern);
		getRoom(BALCONY).addItem(ocean);
		
		Special bed=new Special(this, "large_bed", 0, Item.NOT_TAKEABLE, "A large bed that seems to be very comfortable.", new UselessItem(this, "magnesium", 0, Item.TAKEABLE, "A strip of magnesium"));
		LockableContainer chest=new LockableContainer(this, "chest", 0, Item.NOT_TAKEABLE, "A large, locked chest.", false, true, "wooden_key");
		Maker mortar=new Maker(this, "mortar_and_pestle", 0, Item.TAKEABLE, "A mortar and pestle that can be used to mix different substances.");
		UselessItem iron_oxide=new UselessItem(this, "iron_oxide", 0, Item.TAKEABLE, "A handful of iron oxide");
		UselessItem magnesium=new UselessItem(this, "magnesium", 0, Item.TAKEABLE, "A strip of magnesium");
		UselessItem aluminum_powder=new UselessItem(this, "aluminum_powder", 0, Item.TAKEABLE, "A handful of aluminum powder");
		mortar.addItem(iron_oxide);
		mortar.addItem(magnesium);
		mortar.addItem(aluminum_powder);
		mortar.setResult(new Thermite(this, "thermite", 0, Item.TAKEABLE, "You have a handful of reddish powder."));
		Scenery bunks=new Scenery(this, "bunk_beds", 0, "Many bunk beds are up against the walls of this room.");
		CloseableContainer cont8=new CloseableContainer(this, "cabinets", 0, Item.NOT_TAKEABLE, "The cabinets that the crew uses to store their belonngings", false);
		UselessItem clothes=new UselessItem(this, "dirty_clothes", 0, Item.NOT_TAKEABLE, "The dirty clothes of many sailors.");
		Scenery sign=new Scenery(this, "sign", 0, "A sign that reads, \"Storage\".");
		chest.addItem(mortar);
		cont8.addItem(clothes);
		getRoom(CQ).addItem(bed);
		getRoom(CQ).addItem(chest);
		getRoom(CQ).addItem(bunks);
		getRoom(CQ).addItem(cont8);
		getRoom(CQ).addItem(sign);
		
		CloseableContainer cont9=new CloseableContainer(this, "crates", 0, Item.NOT_TAKEABLE, "You think that these are crates, but you are not sure.", false);
		UselessItem supplies=new UselessItem(this, "supplies", 0, Item.NOT_TAKEABLE, "Various supplies, including ammunition, food, and clothes.");
		cont9.addItem(supplies);
		getRoom(STORAGE).addItem(cont9);
		
		Escape boat=new Escape(this, "lifeboat", 0, Item.NOT_TAKEABLE, "A wooden lifeboat.");
		Trap cannons=new Trap(this, "cannon", 0, Item.NOT_TAKEABLE, "A cannon that can fire cannonballs.", "You tried to use the cannon, but it blue up the ship, and you sunk with it.");
		Trap barrels=new Trap(this, "barrels", 0, Item.NOT_TAKEABLE, "A barrel that seems to be filled with rum.", "You had too much rum and fell unconcious on the deck. The\n"
				+ "guard found you and dragged you back into your cell.");
		getRoom(DECK).addItem(boat);
		getRoom(DECK).addItem(cannons);
		getRoom(DECK).addItem(barrels);
	} 

	@Override
	public void printWelcome() {
		World.print("You have been wrongly accused of murder by your previous best friend, \n"
				+ "Norman, and are now stuck on a ship, being transported to a prison in \n"
				+ "Aftokrator. You managed to snatch the guard�s keys while she wasn�t looking,\n"
				+ "but you don�t know when she�s going to notice that her keys are gone. Before\n"
				+ "the guard notices that her keys are gone and comes to find you, make your way\n"
				+ "to the deck of the ship and escape to safety.\n\n");
		print("Welcome to Naval Breakout!\n");
		print("(c) 2020 By Jay Kwon\n");
		print("Type 'help' if you need help.\n\n");
		print(getPlayer().getCurrentRoom().getDescription());
	}

	@Override
	public void onCommandFinished() {
		
	}
}
