package items;

import textadventure.World;

public class Escape extends Item {

	public Escape(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		getWorld().print("You used the " + getName() + " to escape and made it safely\n"
				+ "back home. You win!\n\n");
		getWorld().setGameOver(true);
	}
}
