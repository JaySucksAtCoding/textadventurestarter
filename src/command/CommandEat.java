package command;

import interfaces.Edible;
import textadventure.World;

public class CommandEat extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"eat", "consume", "devour"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if (params.length!=1) {
		    world.print("What do you want to eat?\n\n");
		    return;
		}
		if (!world.getPlayer().hasItem(params[0])) {
			world.print("You don't have the " + params[0] + "\n\n");
			return;
		}
		else {
		    if (world.getPlayer().getItem(params[0]) instanceof Edible)
		    	((Edible)world.getPlayer().getItem(params[0])).doEat();
			else
			    world.print("That's plainly inedible!\n\n");
		}
		
	}

	@Override
	public String getHelpDescription() {
		return "eat [item]";
	}

	
}
