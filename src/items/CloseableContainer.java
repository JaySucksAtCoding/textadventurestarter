package items;

import java.util.HashMap;

import interfaces.Closeable;
import textadventure.World;

public class CloseableContainer extends Container implements Closeable {

	private boolean isOpen;
	
	public CloseableContainer(World world, String name, String description, boolean isOpen) {
		super(world, name, 0, Item.NOT_TAKEABLE, description);
		this.isOpen=isOpen;
	}

	public CloseableContainer(World world, String name, int weight, boolean takeable, String description, boolean isOpen) {
		super(world, name, weight, takeable, description);
		this.isOpen=isOpen;
	}

	@Override
	public boolean isOpen() {
		return isOpen;
	}

	@Override
	public void doOpen() {
		isOpen=Closeable.OPEN;
		super.getWorld().print("Opened.\n\n");
		
	}

	@Override
	public void doClose() {
		isOpen=Closeable.CLOSED;
		super.getWorld().print("Closed");
	}
	
	@Override
	public void doExamine() {
		if(!isOpen) {
			super.getWorld().print("The " + super.getName() + " is closed.\n\n");
		}
		else {
			super.getWorld().print( "Inside the " + super.getName() + " you see " + super.getItemString() + "\n\n");
		}
	}
	
	@Override
	public Item doTake(Item item) {
		if(!isOpen) {
			super.getWorld().print("The " + super.getName() + " is closed.\n\n");
			return null;
		}
		else {
			return super.doTake(item);
		}
	}
	
	@Override
	public Item doPut(Item item, Container source) {
		if(!isOpen) {
			super.getWorld().print("The " + super.getName() + " is closed");
			return null;
		}
		else {
			return super.doTake(item);
		}
	}
}
