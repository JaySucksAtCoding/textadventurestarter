package items;

import textadventure.World;

public class Thermite extends Item {

	public Thermite(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		if (super.getWorld().getPlayer().getCurrentRoom().getName().equals("Hallway")) {
		    getWorld().getRoom("Deck").doUnlock(); 
		    getWorld().print("You unlock the deck.\n\n");
		}
		else {
			getWorld().print("You ignited the thermite, which caused a chain\n"
					+ "reaction that blew up the entire ship. Game Over!\n\n");
			getWorld().setGameOver(true);
		}
	}
	

}
