package items;

import textadventure.World;

public class Book extends Item {
	
	private String text;

	public Book(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		World.print(text + "\n\n");
	}
	
	public void setText(String text) {
		this.text=text;
	}
	
	public String getText() {
		return text;
	}
}
