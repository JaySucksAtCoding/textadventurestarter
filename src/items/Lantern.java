package items;

import textadventure.World;

public class Lantern extends Item {

	private int durability;
	
	public Lantern(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		durability=3;
	}

	@Override
	public void doUse() {
		if(getWorld().getPlayer().getCurrentRoom().getName().equals("Storage")) {
			getWorld().getPlayer().getCurrentRoom().setLit(true);
			getWorld().getPlayer().getCurrentRoom().setDescription(
					"You used the lantern to light up the storage room. You can now see. The\n"
					+ "storage room is filled with dusty crates and barrels. You spot spiderwebs\n"
					+ "in the corners of the room and see little mice scurrying around the floor.\n\n");
			getWorld().getPlayer().getCurrentRoom().getItem("crates").setDescription("There are many crates stacked on one another.");
			getWorld().getPlayer().getCurrentRoom().addItem(new Special(getWorld(), "cobwebs", 0, Item.NOT_TAKEABLE, "The home of a spider.", new UselessItem(getWorld(), "aluminum_powder", 0, Item.TAKEABLE, "A handful of aluminum powder")));
			getWorld().print("You lit up the room. You should take another \"look\" at it.\n\n");
		}
		else {
			getWorld().print("The room is already lit up.\n\n");
		}
	}

	@Override 
	public void doTake(Container container) {
		if (isTakeable()) {
			getWorld().getPlayer().addItem(this);
			container.removeItem(this);
			World.print("Taken.\n\n");
		}
		else {
			durability--;
			if(durability==0) {
				World.print("The lamp becomse loose and comes off of its bidings.\n\n");
				setTakeable(Item.TAKEABLE);
			}
			else {
				World.print("The lamp loosens slightly.\n\n");
			}
		}
	}
}
