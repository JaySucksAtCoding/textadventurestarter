package command;

import items.Item;
import items.Container;
import textadventure.World;

public class CommandTake extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"take", "get", "grab", "hold"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if(world.getPlayer().getCurrentRoom().getLit()) {
			if (params.length == 1)  {
				String itemName = params[0];
				if (world.getPlayer().getCurrentRoom().hasItem(itemName)) {
					Item item = world.getPlayer().getCurrentRoom().getItem(itemName);
					item.doTake(world.getPlayer().getCurrentRoom());
				} else if (world.getPlayer().hasItem(itemName)) {
					World.print("You already have that!\n\n");
				} else {
					World.print("You can't see any " + itemName + " here.\n\n");
				}
			}
			else if(params.length==3) {
				if(!params[1].equals("from")) {
					World.print("I don't understand.\n\n");
					return;
				}
				if(!world.getPlayer().hasItem(params[2]) && !world.getPlayer().getCurrentRoom().hasItem(params[2])) {
					World.print("You can't see any " + params[2] + " here.\n\n");
					return;
				}
				Item item;
				if(world.getPlayer().hasItem(params[2])) {
					item=world.getPlayer().getItem(params[2]);
				}
				else {
					item=world.getPlayer().getCurrentRoom().getItem(params[2]);
				}
				if(!(item instanceof Container)) {
					World.print("The " + params[2] + "can't hold things.\n\n");
					return;
				}
				if(!((Container) item).hasItem(params[0])) {
					World.print("The " + params[2] + " does not have " + params[0] + ".\n\n");
					return;
				}
				((Container)item).doTake(((Container)item).getItem(params[0]));
			}
			else {
				World.print("I don't understand.\n\n");
			}
		}
		else {
			World.print("You can't see anything!\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
	    return "[item] or [item] from [container]";
	}

}
