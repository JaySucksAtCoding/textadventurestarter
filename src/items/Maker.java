package items;

import java.util.ArrayList;

import textadventure.World;

public class Maker extends Item {
	
	private ArrayList<Item>req;
	private Item result;

	public Maker(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
		req=new ArrayList<>();
	}

	@Override
	public void doUse() {
		for(Item ui : req) {
			if(!getWorld().getPlayer().hasItem(ui.getName())) {
				World.print("You do not have the required materials.\n\n");
				return;
			}
		}
		for(Item ui : req) {
			getWorld().getPlayer().removeItem(getWorld().getPlayer().getItem(ui.getName()));
		}
		getWorld().getPlayer().addItem(result);
		World.print("You made " + result.getName() + "!\n\n");
	}
	
	public ArrayList<Item> getReq() {
		return req;
	}
	
	public void addItem(Item i) {
		req.add(i);
	}
	
	public void removeItem(int i) {
		req.remove(i);
	}
	
	public void setResult(Item i) {
		result=i;
	}
	
	public Item getResult() {
		return result;
	}

	
}
