package items;

import textadventure.World;

public class Toaster extends Container{

	public Toaster(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		if (numItems()>1) {
	        World.print("There are too many items in the " + getName() + "\n\n");
	        return;
		}
	    if (numItems()==0) {
	        World.print("There's nothing in the " + getName() + " to toast");
	        return;
	    }
	    if (getItems().get(0).getName().equals("bread")) {
	        World.print("You toast the bread, transforming it into...");
	        removeItem(getItem("bread"));
	        addItem(new Food(getWorld(), "toast", 2, true, "toasted toast"));
	    }
	    else {
	        World.print("You attempt to toast the " + getItems().get(0).getName() + " and burned the house down!  Game over :(");
	        System.exit(0);
	    }
	}
}
