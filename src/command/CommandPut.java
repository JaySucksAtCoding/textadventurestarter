package command;

import items.Item;
import items.Container;
import textadventure.World;

import textadventure.World;

public class CommandPut extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"put"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if(world.getPlayer().getCurrentRoom().getLit()) {
			// Handle errors
			if (params.length!=3 || !cmd.equals("put") || params[1].equals("in")) {
			    world.print("Invalid syntax");
			    return;
			}
			if (world.getPlayer().hasItem(params[0]) && !world.getPlayer().getCurrentRoom().hasItem(params[0])) {
			    world.print("You can't see any "+params[0]+" here!");
			    return;
			}
			if ((world.getPlayer().hasItem(params[2]) && !world.getPlayer().getCurrentRoom().hasItem(params[2]))) {
			    world.print("You can't see any "+params[2]+" here!");
			    return;
			}
			Item item;
			if(world.getPlayer().hasItem(params[2])) {
				item=world.getPlayer().getItem(params[2]);
			}
			else {
				item=world.getPlayer().getCurrentRoom().getItem(params[2]);
			}
			if(!(item instanceof Container)) {
				World.print("The " + params[2] + "can't hold things");
				return;
			}
			if (params[0].equals(params[2])) {
			    world.print("You can't put the " + params[0] + " into itself!");
			    return;
			}
			// If we made it this far, then it's safe to put [item]
			// in [container]
	
			if (world.getPlayer().hasItem(params[0]))  {
				((Container)item).doPut(world.getPlayer().getItem(params[0]), world.getPlayer());
			}
			else {// current room has the item
				((Container)item).doPut(world.getPlayer().getCurrentRoom().getItem(params[0]), world.getPlayer().getCurrentRoom());
			}
		}
		else {
			World.print("You can't see anything!\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		return "[item] into [container]";
	}

}
