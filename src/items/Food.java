package items;

import interfaces.Edible;
import textadventure.World;

public class Food extends Item implements Edible {

	public Food(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doEat() {
		getWorld().getPlayer().setBrushed(false);
		super.getWorld().print("Eating the " + super.getName() + " made you feel stronger.");		
		super.getWorld().getPlayer().setHealth(super.getWorld().getPlayer().getHealth()+getWeight());
		super.getWorld().getPlayer().removeItem(super.getName());
	}

	@Override
	public void doUse() {
		doEat();
	}

}
