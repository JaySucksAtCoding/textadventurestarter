package command;

import items.Item;
import items.Keys;
import items.LockableContainer;
import textadventure.World;

public class CommandUnlock extends Command {

	@Override
	public String[] getCommandWords() {
		return new String[]{"unlock"};
	}

	@Override
	public void doCommand(String cmd, String[] params, World world) {
		if(world.getPlayer().getCurrentRoom().getLit()) {
			if(params.length!=3) {
				World.print("I don't understand.\n\n");
				return;
			}
			if(!params[1].equals("with")) {
				World.print("I don't understand.\n\n");
				return;
			}
			if(!world.getPlayer().hasItem(params[2])) {
				World.print("You don't have " + params[2] + ".\n\n");
				return;
			}
			if(!(world.getPlayer().getItem(params[2]) instanceof Keys)) {
				World.print("The " + params[2] + " can not be used to unlock things.\n\n");
				return;
			}
			if(params[0].equals("north") || params[0].equals("south") || params[0].equals("east") || params[0].equals("west")) {
				if(!world.getPlayer().getCurrentRoom().getExits().contains(params[0])) {
					World.print("There is no room to the " + params[0] + ".\n\n");
					return;
				}
				if(!world.getPlayer().getCurrentRoom().nextRoom(params[0]).isLocked()) {
					World.print("That room is already unlocked.\n\n");
					return;
				}
				if(world.getPlayer().getCurrentRoom().nextRoom(params[0]).getUnlocker().equals(params[2])) {
					world.getPlayer().getCurrentRoom().nextRoom(params[0]).doUnlock();
					World.print("You used the " + params[2] + " to unlock the room to the " + params[0] + ".\n\n");
					return;
				}
				else {
					World.print("The key doesn't fit!\n\n");
					return;
				}
			}
			else {
				if(!world.getPlayer().hasItem(params[0]) && !world.getPlayer().getCurrentRoom().hasItem(params[0])) {
					World.print("You can't see any " + params[0] + " here.\n\n");
					return;
				}
				Item item;
				if(world.getPlayer().hasItem(params[0])) {
					item=world.getPlayer().getItem(params[0]);
				}
				else {
					item=world.getPlayer().getCurrentRoom().getItem(params[0]);
				}
				if(!(item instanceof LockableContainer)) {
					World.print("You can't unlock " + params[0] + ".\n\n");
					return;
				}
				if(!((LockableContainer)item).isLocked()) {
					World.print("This container is already unlocked.\n\n");
					return;
				}
				if(((LockableContainer)item).getUnlocker().equals(params[2])) {
					((LockableContainer)item).doUnlock();
					World.print("You used the " + params[2] + " to unlock the " + params[0] + ".\n\n");
					return;
				}
				else {
					World.print("The key doesn't fit!\n\n");
					return;
				}
			}
		}
		else {
			World.print("You can't see anything!\n\n");
		}
	}

	@Override
	public String getHelpDescription() {
		return "unlock [direction/container] with [key]";
	}

}
