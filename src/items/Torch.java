package items;

import textadventure.World;

public class Torch extends Item {

	public Torch(World world, String name, int weight, boolean takeable, String description) {
		super(world, name, weight, takeable, description);
	}

	@Override
	public void doUse() {
		getWorld().getPlayer().getCurrentRoom().setLit(false);
		getWorld().print("You accidentally put the torch out! It is now very dark and you can not see anything.\n\n");
		setDescription("A wooden torch taht was put out by someone.");
	}

}
