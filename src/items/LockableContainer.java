package items;

import interfaces.Closeable;
import interfaces.Lockable;
import textadventure.World;

public class LockableContainer extends CloseableContainer implements Lockable {
	
	private boolean isLocked;
	private String unlocker;

	public LockableContainer(World world, String name, int weight, boolean takeable, String description, boolean isOpen, boolean isLocked, String unlocker) {
		super(world, name, weight, takeable, description, isOpen);
		this.isLocked=isLocked;
		this.unlocker=unlocker;
		
	}

	@Override
	public boolean isLocked() {
		return isLocked;
	}

	@Override
	public void doLock() {
		isLocked=true;
	}

	@Override
	public void doUnlock() {
		isLocked=false;
	}
	
	@Override
	public void doOpen() {
		if(isLocked()) {
			getWorld().print("Sorry, this cotainer is locked.\n\n");
		}
		else {
			super.doOpen();
		}
	}
	
	public String getUnlocker() {
		return unlocker;
	}
	
	public void setUnlocker(String unlocker) {
		this.unlocker=unlocker;
	}

}
