package items;

import textadventure.World;

public class Special extends Item {
	
	private Item newItem;
	private boolean found;
	
	public Special(World world, String name, int weight, boolean takeable, String description, Item newItem) {
		super(world, name, weight, takeable, description);
		this.newItem=newItem;
		found=false;
	}

	@Override
	public void doUse() {
		doExamine();
	}
	
	@Override
	public void doExamine() {
		World.print(getDescription() + "\n");
		if(!found) {
			World.print("A " + newItem.getName() + " dropped out of the " + getName() + ".\n\n");
			getWorld().getPlayer().getCurrentRoom().addItem(newItem);
			found=true;
		}
	}

}
