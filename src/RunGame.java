import textadventure.DemoWorld;
import textadventure.NavalBreakout;

public class RunGame {
	
	public static void main(String[] args) {
		new NavalBreakout().play();
	}	
}